## INTRODUCTION

The Skribble Integration module allows you to easily integrate the Skribble API into your Drupal site.

The primary use case for this module is:

- [Create a signature request](https://api-doc.skribble.com/#1f4b6619-cc7a-45a6-9a60-f3af877317b5)
- [Redirect the user to skribble.com](https://api-doc.skribble.com/#c8db881b-6dae-4125-9535-95866e9cedf8)
- [Fetch the signed document and save it to the Drupal site](https://api-doc.skribble.com/#e86c27a9-ad05-4b99-a02d-8df67c0276fd)

Every step can be altered / changed ina custom module using the provided hooks.
Please see the API file for more information in the module root folder:
`skribble.api.php`

## REQUIREMENTS

No dependencies are required for this module.

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION
- Go to `/admin/config/skribble/settings` and enter your User and API Key
- Choose if you want to upload the file via Base64 encoded upload or let skribble.com fetch the file from your server

## MAINTAINERS

Current maintainer for Drupal 10:

- ayalon - https://www.drupal.org/u/ayalon

