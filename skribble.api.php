<?php

use Drupal\Core\Entity\EntityInterface;
use Drupal\file\FileInterface;
use Drupal\skribble\Entity\SigningRequest;
use Drupal\user\Entity\User;
use libphonenumber\PhoneNumberFormat;

/**
 * Modifies the signatures array by adding information from the current user.
 *
 * @param array $signatures
 *   The array of signatures to be modified. Passed by reference.
 * @param array $context
 *   The context array containing additional information for altering the signatures.
 *
 * @return void
 */
function hook_skribble_skribble_alter_signatures(array &$signatures, array $context) {
  $signatures = [];

  $current_user = \Drupal::currentUser();
  $user = User::load($current_user->id());

  // In this example we use the "Sign without account" feature:
  // https://api-doc.skribble.com/#0b690074-6801-4257-8c05-89f16a100baa
  $signature['signer_identity_data'] = [
    'email' => $user->getEmail(),
    'language' => 'de',
  ];

  if ($user->get('field_address')->family_name) {
    $signature['signer_identity_data']['last_name'] = $user->get('field_address')->family_name;
  }

  if ($user->get('field_address')->given_name) {
    $signature['signer_identity_data']['first_name'] = $user->get('field_address')->given_name;
  }

  if ($user->get('field_mobile_number')->value) {
    $number = \Drupal::service('telephone_formatter.formatter')->format(
      $user->get('field_mobile_number')->value,
      PhoneNumberFormat::E164,
      'CH'
    );
    $signature['signer_identity_data']['mobile_number'] = str_replace('+', '', $number);
  }

  $signatures[] = $signature;

}

/**
 * Modifies the title of the signing request.
 *
 * @param string $title
 *   The title of the signing request. Passed by reference.
 * @param array $context
 *   The context array containing additional information for altering the title.
 *
 * @return void
 */
function hook_skribble_alter_title(string &$title, array $context) {
  if (!empty($context['entity']) && $context['entity'] instanceof EntityInterface) {
    $title = $context['entity']->label();
  }
}

/**
 * Modifies the message of the signing request.
 *
 * @param string $message
 *   The message of the signing request. Passed by reference.
 * @param array $context
 *   The context array containing additional information for altering the message.
 *
 * @return void
 */
function hook_skribble_alter_message(string &$message, array $context) {
  if (!empty($context['entity']) && $context['entity'] instanceof EntityInterface) {

    $bundle_label = \Drupal::entityTypeManager()
      ->getStorage('node_type')
      ->load($context['entity']->bundle())
      ->label();

    $message = t('Please sign this document (@label).', ['@label' => $bundle_label]);
  }
}

/**
 * Modifies the redirect URL after the signing process.
 *
 * @param string $redirect_url
 *   The redirect URL. Passed by reference.
 * @param \Drupal\skribble\Entity\SigningRequest $entity
 *   The signing request entity.
 * @param array $signing_data
 *   The signing data array.
 * @param array $context
 *   The context array containing additional information for altering the redirect URL.
 *
 * @return void
 */
function hook_skribble_alter_redirect_url(string &$redirect_url, SigningRequest $entity, array $signing_data, array $context) {

  // In this example we extract the sign in request for a single signer without an account.
  $redirect_url = $signing_data['signatures'][0]['signing_url'];
}

/**
 * Reacts to the successfully signed document.
 *
 * @param \Drupal\skribble\Entity\SigningRequest $entity
 *   The signing request entity.
 * @param \Drupal\file\FileInterface $file
 *   The file containing the signed document.
 *
 * @return void
 *
 * @throws \Drupal\Core\Entity\EntityStorageException
 *   If there was an error saving the node.
 */
function hook_skribble_signed_document_download(SigningRequest $entity, FileInterface $file) {

  // You could update the node with the signed document here.
  if ($node = $entity->getRelatedNode()) {
    $node->set('field_signed_document', $file->id());
    $node->save();
  }
}
