<?php

namespace Drupal\skribble;

use Drupal\file\FileInterface;

interface ProtectedLinkGeneratorInterface {

  /**
   * Generates a HMAC protected URL for the given file.
   *
   * @param FileInterface $file
   *   The file, e.g., private://ticket-12345.pdf.
   * @param int $expire
   *   (Optional) The expiry date as a UNIX timestamp.
   *
   * @return \Drupal\Core\Url
   */
  public function generate(FileInterface $file, $expire = NULL);

}
