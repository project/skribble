<?php

namespace Drupal\skribble;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Drupal\file\FileRepositoryInterface;
use Drupal\skribble\Entity\SigningRequest;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Psr\Log\LoggerInterface;

/**
 * Provides methods to communicate with the Skribble API.
 */
class ApiClient {

  use StringTranslationTrait;

  const BASE_URL = 'https://api.skribble.com/v1';

  public function __construct(
    protected Client $httpClient,
    protected ConfigFactoryInterface $configFactory,
    protected ProtectedLinkGeneratorInterface $protectedLinkGenerator,
    protected FileSystemInterface $fileSystem,
    protected FileRepositoryInterface $fileRepository,
    protected ModuleHandlerInterface $moduleHandler,
    protected LoggerInterface $logger,
  ) {
  }

  public function authenticate(): string {
    $access_url = $this->getBaseUrl() . '/access/login';

    try {
      $response = $this->httpClient->request('POST', $access_url, [
        'json' => [
          'username' => $this->configFactory->get('skribble.settings')->get('user'),
          'api-key' => $this->configFactory->get('skribble.settings')->get('key'),
        ],
      ]);
    }
    catch (\Exception $e) {
      $response = $e->getResponse();
      $responseBodyAsString = $response->getBody()->getContents();
      $this->logger->error($responseBodyAsString, ['url' => $access_url]);
      throw new \Exception('Fatal: Could not authenticate');
    }

    if ($response->getStatusCode()) {
      $token = $response->getBody()->getContents();

      return $token;
    }

    throw new \Exception('Fatal: Could not authenticate');
  }

  public function signatureRequest(
    string $title,
    string $message,
    array $signatures,
    FileInterface $file,
    SigningRequest $signingRequest,
    string $quality = NULL,
    string $legislation = NULL
  ): array|null {

    $signature_request = $this->getBaseUrl() . '/signature-requests';

    $data = [
      'title' => $title,
      'message' => $message,
      'signatures' => $signatures,
    ];

    if (!empty($quality)) {
      $data['quality'] = $quality;
    }

    if (!empty($legislation)) {
      $data['legislation'] = $legislation;
    }

    // Success Callback enabled?
    if ($this->configFactory->get('skribble.settings')->get('enable_success_callbacks') === TRUE) {
      $data['callback_success_url'] = $this->getBaseUrl() . '/skribble/callback/success/' . $signingRequest->uuid();
    }

    // Send the file content as base64 if the protected download links are not used.
    if (!$this->configFactory->get('skribble.settings')->get('use_protected_download_links')) {
      $file_data = file_get_contents($file->getFileUri());
      $base64 = base64_encode($file_data);
      $data['content'] = $base64;
    }
    else {
      $file_url = $this->protectedLinkGenerator->generate($file);
      $data['file_url'] = $file_url;
    }

    try {
      $response = $this->httpClient->request(
        'POST',
        $signature_request,
        [
          'json' => $data,
          'headers' => [
            'Authorization' => 'Bearer ' . $this->authenticate(),
          ],
        ]
      );
    }
    catch (ClientException $e) {
      $response = $e->getResponse();
      $responseBodyAsString = $response->getBody()->getContents();
      $this->logger->error($responseBodyAsString, ['url' => $signature_request]);
      throw new \Exception('Fatal: Could not execute signature request');
    }

    if ($response->getStatusCode()) {
      $data = json_decode($response->getBody(), TRUE);

      return $data;
    }
    return NULL;
  }

  public function updateSigningRequest(SigningRequest $signingRequest) {

    $signature_request = $this->getBaseUrl() . '/signature-requests/' . $signingRequest->getRequestId();

    try {
      $response = $this->httpClient->request(
        'GET',
        $signature_request,
        [
          'headers' => [
            'Authorization' => 'Bearer ' . $this->authenticate(),
          ],
        ]
      );
    }
    catch (ClientException $e) {
      $response = $e->getResponse();
      $responseBodyAsString = $response->getBody()->getContents();
      $this->logger->error($responseBodyAsString, ['url' => $signature_request]);
      throw new \Exception('Fatal: Could not update signature request');
    }

    if ($response->getStatusCode() === 200) {
      $data = json_decode($response->getBody(), TRUE);

      $signingRequest->set('status_overall', $data['status_overall']);
      $signingRequest->set('document_id', $data['document_id']);
      $signingRequest->set('response_data', $data);
      $signingRequest->save();
    }

  }

  public function downloadSignedDocument(SigningRequest $signingRequest) {

    $download_file = $this->getBaseUrl() . str_replace('{DOC_ID}', $signingRequest->getDocumentId(), '/documents/{DOC_ID}/content');

    try {
      $response = $this->httpClient->request(
        'GET',
        $download_file,
        [
          'headers' => [
            'Authorization' => 'Bearer ' . $this->authenticate(),
          ],
        ]
      );
    }
    catch (ClientException $e) {
      $response = $e->getResponse();
      $responseBodyAsString = $response->getBody()->getContents();
      $this->logger->error($responseBodyAsString, ['url' => $download_file]);
      throw new \Exception('Fatal: Could not download signed document');
    }

    if ($response->getStatusCode() === 200) {
      // Create folder if not exists
      $directory = 'private://skribble/';
      $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);

      $file_path = tempnam($this->fileSystem->getTempDirectory(), 'skribble_') . '.pdf';
      @file_put_contents($file_path, $response->getBody()->getContents());

      $destination = $directory . $signingRequest->getDocumentId() . '.pdf';
      $this->fileSystem->move($file_path, $destination, TRUE);

      $file = $this->fileRepository->loadByUri($destination);
      if (!$file) {
        $file = File::create([
          'filename' => basename($destination),
          'uri' => $destination,
          'status' => 1,
          'uid' => $signingRequest->getOwnerId(),
        ]);
        $file->save();
      }

      $signingRequest->set('file', $file->id());
      $signingRequest->save();

      $this->moduleHandler->invokeAll('skribble_signed_document_download', [$signingRequest, $file]);
    }

  }

  private function getBaseUrl() {
    return $this->configFactory->get('skribble.settings')->get('base_url') ?? self::BASE_URL;
  }

}
