<?php

namespace Drupal\skribble;

/**
 * Provides methods to generate and check the HMAC for protected downloads
 */
interface SecurityKeyInterface {

  /**
   * Validates a HMAC.
   *
   * @param string $uri
   *   The file uri, e.g., private://ticket-12345.pdf.
   * @param string $expire
   *   A hex-encoded UNIX timestamp.
   * @param string $hmac
   *   The message authentication code.
   *
   * @return bool
   *   TRUE if this download link is still valid, FALSE otherwise.
   */
  public function verify(string $uri, string $expire, string $hmac);

  /**
   * Calculates a base-64 encoded, URL-safe sha-256 HMAC.
   *
   * @param string $uri
   *   The file uri, e.g., private://ticket-12345.pdf.
   * @param string $expire
   *   A hex-encoded UNIX timestamp.
   *
   * @return string
   *   The message authentication code.
   */
  public function generate(string $uri, string $expire);

}
