<?php

declare(strict_types=1);

namespace Drupal\skribble;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a signing request entity type.
 */
interface SigningRequestInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}
