<?php

namespace Drupal\skribble;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManager;
use Drupal\Core\Url;
use Drupal\file\FileInterface;

class ProtectedLinkGenerator implements ProtectedLinkGeneratorInterface {

  /**
   * @var \Drupal\skribble\SecurityKeyInterface
   */
  protected $securityKey;

  /**
   * The request time.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a new link generator.
   *
   * @param \Drupal\skribble\SecurityKeyInterface
   *   The protected download security key service.
   * @param \Drupal\Component\Datetime\TimeInterface
   *   The request time service.
   */
  public function __construct(
    SecurityKeyInterface $security_key,
    TimeInterface $time
  ) {
    $this->securityKey = $security_key;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public function generate(FileInterface $file, $expire = NULL) {
    $uri = $file->getFileUri();
    $result = FALSE;

    $scheme = StreamWrapperManager::getScheme($uri);
    $target = StreamWrapperManager::getTarget($uri);

    if ($scheme !== FALSE && $target !== FALSE) {
      if (!isset($expire)) {
        // Link is exactly 60 seconds valid.
        $expire = $this->time->getRequestTime() + 6000;
      }
      $expire_string = dechex($expire);

      $hmac = $this->securityKey->generate($uri, $expire_string);
      $result = Url::fromRoute('skribble.download_file', [
        'file' => $file->id(),
        'expire' => $expire_string,
        'hmac' => $hmac,
      ]);
    }

    return $result->setAbsolute()->toString();
  }

}
