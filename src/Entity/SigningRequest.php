<?php

declare(strict_types=1);

namespace Drupal\skribble\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\node\NodeInterface;
use Drupal\skribble\SigningRequestInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the signing request entity class.
 *
 * @ContentEntityType(
 *   id = "skribble_signing_request",
 *   label = @Translation("Signing Request"),
 *   label_collection = @Translation("Signing Requests"),
 *   label_singular = @Translation("signing request"),
 *   label_plural = @Translation("signing requests"),
 *   label_count = @PluralTranslation(
 *     singular = "@count signing requests",
 *     plural = "@count signing requests",
 *   ),
 *   handlers = {
 *     "storage_schema" = "Drupal\skribble\Entity\Storage\SkribbleStorageSchema",
 *     "list_builder" = "Drupal\skribble\SigningRequestListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\skribble\SigningRequestAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\skribble\Form\SigningRequestForm",
 *       "edit" = "Drupal\skribble\Form\SigningRequestForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\skribble\Routing\SigningRequestHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "skribble_signing_request",
 *   admin_permission = "administer skribble_signing_request",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *   },
 *   links = {
 *     "collection" = "/admin/content/signing-request",
 *     "add-form" = "/skribble/signing-request/add",
 *     "canonical" = "/skribble/signing-request/{skribble_signing_request}",
 *     "edit-form" = "/skribble/signing-request/{skribble_signing_request}",
 *     "delete-form" = "/skribble/signing-request/{skribble_signing_request}/delete",
 *     "delete-multiple-form" = "/admin/content/signing-request/delete-multiple",
 *   },
 *   field_ui_base_route = "entity.skribble_signing_request.settings",
 * )
 */
final class SigningRequest extends ContentEntityBase implements SigningRequestInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage): void {
    parent::preSave($storage);
    if (!$this->getOwnerId()) {
      // If no owner has been set explicitly, make the anonymous user the owner.
      $this->setOwnerId(0);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Author'))
      ->setSetting('target_type', 'user')
      ->setDefaultValueCallback(self::class . '::getDefaultEntityOwner')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the signing request was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the signing request was last edited.'));

    $fields['file'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('File'))
      ->setDescription(t('The file.'))
      ->setRequired(TRUE)
      ->setSetting('target_type', 'file')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'file_generic',
        'weight' => 5,
      ]);

    $fields['node'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Node'))
      ->setDescription(t('Optional related node.'))
      ->setRequired(FALSE)
      ->setSetting('target_type', 'node')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => -1,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
      ]);

    $fields['signed_document'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Signed document'))
      ->setDescription(t('The signed document.'))
      ->setSetting('target_type', 'file')
      ->setDisplayOptions('form', [
        'type' => 'file_generic',
        'weight' => 5,
      ]);

    $fields['request_id'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('Request ID'))
      ->setRequired(TRUE)
      ->setDescription(t('The request ID.'))
      ->setDisplayOptions('form', [
        'type' => 'uuid_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['document_id'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('Document ID'))
      ->setRequired(TRUE)
      ->setDescription(t('The document ID.'))
      ->setDisplayOptions('form', [
        'type' => 'uuid_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['signing_url'] = BaseFieldDefinition::create('string')
      ->setLabel('Signing URL')
      ->setRequired(TRUE)
      ->setDescription(t('The signing url.'))
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['status_overall'] = BaseFieldDefinition::create('string')
      ->setLabel('Status')
      ->setRequired(TRUE)
      ->setDescription(t('The status.'))
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['quality'] = BaseFieldDefinition::create('string')
      ->setLabel('Quality')
      ->setRequired(TRUE)
      ->setDescription(t('The signing quality.'))
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['destination'] = BaseFieldDefinition::create('string')
      ->setLabel('Destination')
      ->setRequired(TRUE)
      ->setDescription(t('The destination url after signing.'))
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['signatures'] = BaseFieldDefinition::create('map')
      ->setLabel(t('Signatures'))
      ->setRequired(TRUE)
      ->setDescription(t('A serialized array of the signatures.'))
      ->setDisplayOptions('form', [
        'type' => 'map_textarea',
        'weight' => 25,
        'settings' => [
          'rows' => 4,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['response_data'] = BaseFieldDefinition::create('map')
      ->setLabel(t('Response Data'))
      ->setRequired(TRUE)
      ->setDescription(t('A serialized array of the response.'))
      ->setDisplayOptions('form', [
        'type' => 'map_textarea',
        'weight' => 25,
        'settings' => [
          'rows' => 4,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);

    return $fields;
  }

  public function getDestination(): string {
    return $this->get('destination')->value;
  }

  public function getSigningUrl(): string {
    return $this->get('signing_url')->value;
  }

  public function getRequestId(): string {
    return $this->get('request_id')->value;
  }

  public function getDocumentId(): string {
    return $this->get('document_id')->value;
  }

  public function getRelatedNode(): ?NodeInterface {
    return $this->get('node')->entity;
  }

  public function getStatusOverall(): ?string {
    return $this->get('status_overall')->value;
  }

}
