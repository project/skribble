<?php

declare(strict_types=1);

namespace Drupal\skribble\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\node\NodeInterface;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\skribble\ApiClient;
use Drupal\skribble\Entity\SigningRequest;
use Drupal\skribble\SecurityKeyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Returns responses for Skribble routes.
 */
class SkribbleController extends ControllerBase {

  public function __construct(
    protected ApiClient $apiClient,
    protected Request $request,
    protected SecurityKeyInterface $securityKey,
    protected RendererInterface $renderer
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('skribble.api_client'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('skribble.security_key'),
      $container->get('renderer')
    );
  }

  /**
   * Download a protected file.
   *
   * @param \Drupal\file\Entity\File $file
   *   The file to download.
   * @param string $hmac
   *   The HMAC of the file.
   * @param int $expire
   *   The expiration time of the file.
   *
   * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
   *   The response.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   *   Thrown when the file is not found.
   */
  public function downloadProtectedFile(File $file, $hmac, $expire) {
    $response = NULL;
    $uri = $file->getFileUri();
    if ($this->securityKey->verify($uri, $expire, $hmac)) {

      $stat = @stat($uri);
      if (is_array($stat)) {
        $headers = $this->moduleHandler()->invokeAll('file_download', [$uri]);
        $response = new BinaryFileResponse($uri, 200, $headers);
        $response->setPrivate();
      }
    }

    if (isset($response)) {
      return $response;
    }
    else {
      throw new NotFoundHttpException();
    }
  }

  /**
   * Start signing request from node.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node.
   * @param string $field_name
   *   The field name.
   *
   * @return \Drupal\Core\Routing\TrustedRedirectResponse
   *   The response.
   */
  public function startSigningRequestFromNode(NodeInterface $node, $field_name) {
    $context = [
      'entity' => $node,
      'field_name' => $field_name,
    ];
    $file = $node->get($field_name)->entity;

    return $this->startSigningRequest($file, $context);

  }

  /**
   * Start signing request from file.
   *
   * @param \Drupal\file\Entity\File $file
   *   The file.
   *
   * @return \Drupal\Core\Routing\TrustedRedirectResponse
   *   The response.
   */
  public function startSigningRequest(File $file, $context = []) {

    // Create a helpful context for the hooks.
    $context['file'] = $file;

    // Get the signatures to be displayed.
    $signatures = $this->getSignatures($context);

    // Check if mail notification should be sent.
    foreach ($signatures as $key => $signature) {
      if ($this->config('skribble.settings')->get('send_email') !== TRUE) {
        $signatures[$key]['notify'] = FALSE;
      }
    }

    // Create an empty signing request entity to rgab the uuid.
    $entity = SigningRequest::create([]);

    // Do the actual signing request.
    $signing_data = $this->apiClient->signatureRequest(
      $this->getTitle($context),
      $this->getMessage($context),
      $signatures,
      $file,
      $entity,
      $this->config('skribble.settings')->get('quality'),
      $this->config('skribble.settings')->get('legislation'),
    );

    // Save data in entity
    $entity->set('file', $file->id());
    $entity->set('label', $this->getTitle($context));
    $entity->set('request_id', $signing_data['id']);
    $entity->set('document_id', $signing_data['document_id']);
    $entity->set('signing_url', $signing_data['signing_url']);
    $entity->set('status_overall', $signing_data['status_overall']);
    $entity->set('quality', $signing_data['quality']);
    $entity->set('signatures', $signing_data['signatures']);
    $entity->set('response_data', $signing_data);
    $entity->set('uid', $this->currentUser()->id());
    $entity->set('destination', $this->getDestination($context));

    if ($context['entity']) {
      $entity->set('node', $context['entity']->id());
    }

    $this->moduleHandler()->invokeAll('skribble_alter_signing_request_entity', [&$entity, $context]);

    $entity->save();

    $default_signing_url = $entity->getSigningUrl();
    $this->moduleHandler()->invokeAll('skribble_alter_redirect_url', [&$default_signing_url, $entity, $signing_data, $context]);

    // Redirect the user to skribble.com to sign the document.
    $url = Url::fromUri($default_signing_url, ['absolute' => TRUE]);
    $url->setOption('query', [
      'lang' => $this->languageManager()->getCurrentLanguage()->getId(),
      'exitURL' => Url::fromRoute('skribble.finish_signing_request', ['signing_request' => $entity->id()], ['absolute' => TRUE])->toString(TRUE)->getGeneratedUrl(),
      'errorURL' => $this->getDestination($context),
      'declineURL' => $this->getDestination($context),
      'redirectTimeout' => 10,
      'hidedownload' => 'true',
    ]);

    $url_string = $url->toString(TRUE)->getGeneratedUrl();

    $response = new TrustedRedirectResponse($url_string);

    return $response;
  }

  public function finishSigningRequest(SigningRequest $signing_request) {

    $context = new RenderContext();
    $this->renderer->executeInRenderContext($context, function () use ($signing_request): string {
      // Update the signing request.
      $this->apiClient->updateSigningRequest($signing_request);

      $status = $signing_request->getStatusOverall();
      if ($status !== 'SIGNED') {
        $this->messenger()->addError($this->t('There was an error signing the document. Please try again.'));
        return '';
      }

      // Download the signed document.
      $this->apiClient->downloadSignedDocument($signing_request);
      return '';
    });

    $response = new TrustedRedirectResponse($signing_request->getDestination());

    return $response;
  }

  public function successCallback($signing_request_uuid) {

    $signing_request = $this->entityTypeManager()->getStorage('skribble_signing_request')->loadByProperties(['uuid' => $signing_request_uuid]);
    $signing_request = reset($signing_request);

    if (!$signing_request) {
      throw new AccessDeniedHttpException();
    }

    // Update the signing request.
    $this->finishSigningRequest($signing_request);

    $response = new ModifiedResourceResponse(NULL, 200);

    return $response;
  }

  /**
   * Gets the signatures to be displayed.
   *
   * @param array $context
   *   An associative array containing the context for the signatures.
   *
   * @return array
   *   The signatures to be used.
   */
  private function getSignatures(array $context) {

    // The default signature is the current user.
    $signatures[] = [
      'signer_email_address' => $this->currentUser()->getEmail(),
    ];

    // Allow other modules to alter the signatures.
    $this->moduleHandler()->invokeAll('skribble_alter_signatures', [&$signatures, $context]);

    return $signatures;
  }

  /**
   * Gets the title to be displayed.
   *
   * @param array $context
   *   An associative array containing the context for the title.
   *
   * @return string
   *   The title to be displayed.
   */
  private function getTitle(array $context) {
    $title = $this->t('Please sign this document.');

    $this->moduleHandler()->invokeAll('skribble_alter_title', [&$title, $context]);

    return $title;
  }

  /**
   * Gets the message to be displayed.
   *
   * @param array $context
   *   An associative array containing the context for the message.
   *
   * @return string
   *   The message to be displayed.
   */
  private function getMessage(array $context) {
    $message = $this->t('Please sign this document.');

    $this->moduleHandler()->invokeAll('skribble_alter_message', [&$message, $context]);

    return $message;
  }

  private function getDestination(array $context) {

    // Fallback to the front page.
    $destination_url = Url::fromRoute('<front>', [], ['absolute' => TRUE]);

    // If destination is set, use it.
    if ($this->request->query->get('destination')) {
      $destination = $this->getRedirectDestination()->get();
      $destination_url = Url::fromUserInput($destination, ['absolute' => TRUE]);
    }

    if (!empty($context['entity']) && $context['entity'] instanceof EntityInterface) {
      $destination_url = $context['entity']->toUrl('canonical', ['absolute' => TRUE]);
    }

    $this->moduleHandler()->invokeAll('skribble_alter_destination', [&$destination_url, $context]);

    // Return the absolute url.
    return $destination_url->toString(TRUE)->getGeneratedUrl();
  }

}
