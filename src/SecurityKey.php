<?php

namespace Drupal\skribble;

use Drupal\Component\Datetime\Time;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Site\Settings;

/**
 * Provides methods to generate and check the HMAC for protected downloads
 */
class SecurityKey implements SecurityKeyInterface {

  /**
   * HMAC key.
   *
   * @var string
   */
  protected $hmacKey;

  /**
   * The request time.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a download security key service.
   *
   * @param \Drupal\Component\Datetime\TimeInterface
   *   The request time service.
   */
  public function __construct(
    Time $time,
    $hmac_key = NULL
  ) {
    $this->hmacKey = $hmac_key ?? \Drupal::service('private_key')->get() . Settings::getHashSalt();
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public function verify(string $uri, string $expire, string $hmac) {
    $result = FALSE;

    if (strlen($uri) && strlen($expire) && strlen($hmac)) {
      $calculated_hmac = Crypt::hmacBase64($uri . $expire, $this->hmacKey);
      $result = hash_equals($calculated_hmac, $hmac) &&
        $this->time->getRequestTime() <= hexdec($expire);
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function generate(string $uri, string $expire) {
    return Crypt::hmacBase64($uri . $expire, $this->hmacKey);
  }

}
