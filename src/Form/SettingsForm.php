<?php

declare(strict_types=1);

namespace Drupal\skribble\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Configure Skribble settings for this site.
 */
final class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'skribble_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['skribble.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $form['api_base_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Base URL'),
      '#required' => TRUE,
      '#default_value' => $this->config('skribble.settings')->get('api_base_url') ?? 'https://api.skribble.com/v1',
    ];

    $form['user'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API User'),
      '#required' => TRUE,
      '#default_value' => $this->config('skribble.settings')->get('user'),
    ];

    $form['key'] = [
      '#type' => 'password',
      '#title' => $this->t('API Key'),
      '#required' => empty($this->config('skribble.settings')->get('key')) ? TRUE : FALSE,
      '#default_value' => $this->config('skribble.settings')->get('key'),
    ];

    $form['quality'] = [
      '#type' => 'select',
      '#title' => $this->t('Signature Quality'),
      '#options' => [
        'QES' => $this->t('QES'),
        'AES' => $this->t('AES'),
        'AES_MINIMAL' => $this->t('AES_MINIMAL'),
        'SES' => $this->t('SES'),
        'DEMO' => $this->t('DEMO'),
      ],
      '#required' => TRUE,
      '#description' => $this->t('QES: The document must be signed at QES, so every signer must use QES. This is the default quality. <br/>
      AES: The document should be signed at AES. Signers will use AES, if available, otherwise they fall back to QES, if available. <br/>
      AES_MINIMAL: The document will be signed at at least AES, but a higher level is preferred. Signers will sign QES, if available, otherwise they will use AES. <br/>
      SES: The document will be signed at SES by every signer.. <br/>
      DEMO: The document will be signed with a DEMO signature by every signer.
      Note: This signature has NO legal binding at all.
      ALL SignatureRequests that are created using an API demo user (api_demo_example_xyz) will get this quality by default. <br/>
      '),
      '#default_value' => $this->config('skribble.settings')->get('quality'),
    ];

    $form['legislation'] = [
      '#type' => 'select',
      '#title' => $this->t('Legislation'),
      '#options' => [
        'ZERTES' => $this->t('ZERTES: QES according to the Swiss law. This is the default legislation.'),
        'EIDAS' => $this->t('EIDAS: QES according to the EU law.'),
      ],
      '#required' => TRUE,
      '#default_value' => $this->config('skribble.settings')->get('legislation'),
    ];

    $form['send_email'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send e-mail notification'),
      '#description' => $this->t('If checked, an e-mail notification will be sent to the user a signing request is initiated.'),
      '#default_value' => $this->config('skribble.settings')->get('send_email') ?? FALSE,
    ];

    $form['use_protected_download_links'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use protected download links'),
      '#description' => $this->t('Instead of sending the file Base64 encoded in the signing request, a protected download link (HMAC and 60 seconds expiry time) will be used. This is recommended for large files.'),
      '#default_value' => $this->config('skribble.settings')->get('use_protected_download_links') ?? TRUE,
    ];

    $form['enable_success_callbacks'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Skribble will call back to this site on success'),
      '#description' => $this->t('If checked, Skribble will call back to this site when a document was successfully signed.'),
      '#default_value' => $this->config('skribble.settings')->get('enable_success_callbacks') ?? TRUE,
    ];

    $form['link'] = [
      '#type' => 'link',
      '#title' => $this->t('Where can I find the Skribble API key?'),
      '#attributes' => [
        'target' => '_blank',
      ],
      '#url' => Url::fromUri('https://docs.skribble.com/business-admin/api/apicreate.html'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {

    $base_url = rtrim($form_state->getValue('api_base_url'), '/');

    $this->config('skribble.settings')
      ->set('mode', $base_url)
      ->set('user', $form_state->getValue('user'))
      ->set('quality', $form_state->getValue('quality'))
      ->set('legislation', $form_state->getValue('legislation'))
      ->set('send_email', $form_state->getValue('send_email'))
      ->set('use_protected_download_links', $form_state->getValue('use_protected_download_links'))
      ->set('enable_success_callbacks', $form_state->getValue('enable_success_callbacks'))
      ->save();

    if ($this->config('skribble.settings')->get('key') !== $form_state->getValue('key')) {
      if (!empty($form_state->getValue('key'))) {
        $this->config('skribble.settings')
          ->set('key', $form_state->getValue('key'))
          ->save();
      }
    }
    parent::submitForm($form, $form_state);

  }

}
